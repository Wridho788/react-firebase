import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';
import { getStorage } from 'firebase/storage';

const firebaseConfig = {
  apiKey: 'AIzaSyDoROjb8ukSPV7ebrhkFko657-o9m9SVKA',
  authDomain: 'spk-smart-6eebd.firebaseapp.com',
  projectId: 'spk-smart-6eebd',
  storageBucket: 'spk-smart-6eebd.appspot.com',
  messagingSenderId: '381501676900',
  appId: '1:381501676900:web:b5950352a2022824972e71',
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth();
export const db = getFirestore(app);
export const storage = getStorage(app);
