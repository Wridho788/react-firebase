import Home from './pages/home/Home';
import Login from './pages/login/Login';
import List from './pages/list/List';
import Single from './pages/single/Single';
import New from './pages/new/New';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import { productInputs, userInputs } from './formSource';
import './style/dark.scss';
import { useContext } from 'react';
import { DarkModeContext } from './context/darkModeContext';
import { AuthContext } from './context/AuthContext';

function App() {
  const { darkMode } = useContext(DarkModeContext);
  const { currentUser } = useContext(AuthContext);

  const RequeireAuth = ({ children }) => {
    return currentUser ? children : <Navigate to='/login' />;
  };

  return (
    <div className={darkMode ? 'app dark' : 'app'}>
      <BrowserRouter>
        <Routes>
          <Route path='/'>
            <Route path='login' element={<Login />} />
            <Route
              index
              element={
                <RequeireAuth>
                  <Home />
                </RequeireAuth>
              }
            />
            <Route path='users'>
              <Route
                index
                element={
                  <RequeireAuth>
                    <List />
                  </RequeireAuth>
                }
              />
              <Route
                path=':userId'
                element={
                  <RequeireAuth>
                    <Single />
                  </RequeireAuth>
                }
              />
              <Route
                path='new'
                element={
                  <RequeireAuth>
                    <New inputs={userInputs} title='Add New User' />
                  </RequeireAuth>
                }
              />
            </Route>
            <Route path='products'>
              <Route
                index
                element={
                  <RequeireAuth>
                    <List />
                  </RequeireAuth>
                }
              />
              <Route
                path=':productId'
                element={
                  <RequeireAuth>
                    <Single />
                  </RequeireAuth>
                }
              />
              <Route
                path='new'
                element={
                  <RequeireAuth>
                    <New inputs={productInputs} title='Add New Product' />
                  </RequeireAuth>
                }
              />
            </Route>
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
